package com.example.football;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.media.Image;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    static public int teamFirst = 0;
    static public int teamSecond = 0;
    static public TextView pointTeamFirst;
    static public TextView pointTeamSecond;
    ImageView imgBtnTeamFirst,imgBtnTeamSecond,imgBtnZeroing;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         imgBtnTeamFirst = (ImageView) findViewById(R.id.imageTeam1);
         imgBtnTeamSecond = (ImageView) findViewById(R.id.imageTeam2);
        imgBtnZeroing = (ImageView) findViewById(R.id.zeroing);
        pointTeamFirst = (TextView) findViewById(R.id.pointTeamFirst);
        pointTeamSecond = (TextView) findViewById(R.id.pointTeamSecond);
        btnClick();
    }
    void btnClick(){
        imgBtnTeamFirst.setClickable(true);
        imgBtnTeamFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                teamFirst++;
                pointTeamFirst.setText(teamFirst + " ");
            }
        });
        imgBtnTeamSecond.setClickable(true);
        imgBtnTeamSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                teamSecond++;
                pointTeamSecond.setText(teamSecond + " ");
            }
        });
        imgBtnZeroing.setClickable(true);
        imgBtnZeroing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                teamFirst = 0;
                teamSecond = 0;
                pointTeamFirst.setText(teamFirst + " ");
                pointTeamSecond.setText(teamSecond + " ");
            }
        });
    }
}
